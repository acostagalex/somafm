#!/usr/bin/python


import os
import subprocess


def menu():
    """
    Clear screen and show menu
    """
    os.system('clear')
    print("Select Soma FM station\n\n")
    print("\t1 - Deep Space One")
    print("\t2 - Sonic Universe")
    print("\t3 - Space Station")
    print("\t4 - Dub Step Beyond")
    print("\t5 - Drone Zone") 
    print("\t6 - Mission Control")
    print("\t7 - SF 10-33")
    print("\t8 - Heavyweight Reggae")
    print("\n\n\te - exit\n\n")


while True:
    # Show menu
    menu()

    optionMenu = input("Select Soma FM Station >> ")

    if optionMenu == 1:
        print("You select Deep Space One")
        os.system('/usr/bin/mplayer http://ice1.somafm.com/deepspaceone-128-mp3 -cache 4096')
#        subprocess.run(["/usr/bin/mplayer", "http://ice1.somafm.com/deepspaceone-128-mp3", "-cache", "4096"])

    elif optionMenu == 2:
        print("You select Sonic Universe")
        os.system('/usr/bin/mplayer http://ice1.somafm.com/sonicuniverse-128-mp3 -cache 4096')

    elif optionMenu == 3:
        print("You select Space Station")
        os.system('/usr/bin/mplayer http://ice1.somafm.com/spacestation-128-mp3 -cache 4096')

    elif optionMenu == 4:
        print("You select Dub Step Beyond")
        os.system('/usr/bin/mplayer http://ice1.somafm.com/dubstep-256-mp3 -cache 4096')

    elif optionMenu == 5:
        print("You select Drone Zone")
        os.system('/usr/bin/mplayer http://ice1.somafm.com/dronezone-256-mp3 -cache 4096')

    elif optionMenu == 6:
        print("You select Mission Control")
        os.system('/usr/bin/mplayer http://ice1.somafm.com/missioncontrol-128-mp3 -cache 4096')
    
    elif optionMenu == 7:
        print("You select SF 10-33")
        os.system('/usr/bin/mplayer http://ice1.somafm.com/sf1033-128-mp3 -cache 4096')
    
    elif optionMenu == 8:
        print("You select Heavyweight Reggae")
        os.system('/usr/bin/mplayer http://ice6.somafm.com/reggae-256-mp3 cache 4096')


    elif optionMenu == "e":
        break

    else:
        print("")
        input("Pulsa una tecla para continuar")
